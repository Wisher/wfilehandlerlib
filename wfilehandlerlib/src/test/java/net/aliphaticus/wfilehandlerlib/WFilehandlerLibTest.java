package net.aliphaticus.wfilehandlerlib;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WFilehandlerLibTest {

    @Test void testGetDirectory() {
        assertEquals(WFilehandlerLib.getDirectory(), System.getProperty("user.dir"));
    }

}
