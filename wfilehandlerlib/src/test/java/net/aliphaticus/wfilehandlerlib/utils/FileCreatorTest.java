package net.aliphaticus.wfilehandlerlib.utils;

import net.aliphaticus.wfilehandlerlib.WFilehandlerLib;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class FileCreatorTest {

    private static FileCreator fileCreator;
    private static File file1;
    private static File file2;
    private static File file3;

    @BeforeAll static void initAll() {
        fileCreator = new FileCreator();
    }



    @Test void testNewFileFilePathTrue() {
        file1 = fileCreator.newFile(WFilehandlerLib.getDirectory() + "/" + "testCreatorFilePath");
        assertTrue(file1.exists());
    }

    @Test void testNewFileDirNameTrue() {
        file2 = fileCreator.newFile(WFilehandlerLib.getDirectory(), "testCreatorDirName");
    }

    @Test void testNewFileFromPathTrue() {
        file3 = fileCreator.newFileFromPath("testCreatorFromPath");
    }



    @AfterAll static void tearDownAll() {
        file1.delete();
        file2.delete();
        file3.delete();
    }


}
