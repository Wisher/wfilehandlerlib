package net.aliphaticus.wfilehandlerlib.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FileWriterTest {

    @Test
    void testIsWriterOpenTrue() {
        File file = new FileCreator().newFileFromPath("testFileWriterOpenTrue");
        FileWriter fileWriter = new FileWriter(file);

        assertTrue(fileWriter.isWriterOpen());
        fileWriter.closeWriter();
        file.delete();
    }

    @Test
    void testIsWriterOpenFalse() {
        File file = new FileCreator().newFileFromPath("testFileWriterOpenFalse");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.closeWriter();

        assertFalse(fileWriter.isWriterOpen());
        file.delete();
    }

    @Test
    void testGetWriterTrue() {
        File file = new FileCreator().newFileFromPath("testFileWriterGetWriter");
        FileWriter fileWriter = new FileWriter(file);

        assertTrue(fileWriter.getWriter() instanceof java.io.FileWriter);
        fileWriter.closeWriter();
        file.delete();
    }

    @Test
    void testResetWriterTrue() {
        File file = new FileCreator().newFileFromPath("testFileWriterResetWriter");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.writeLine("test");
        fileWriter.resetWriter();
        fileWriter.closeWriter();

        final FileReader fileReader = new FileReader(file); // local variables in lambdas must be final

        assertAll(
                () -> assertNull(fileReader.readNextLine()),
                () -> assertEquals(Arrays.toString(new String[] {}), Arrays.toString(fileReader.readFile()))
        );

        fileReader.closeFile();
        file.delete();
    }

    @Test
    void testWriteLineTrue() {
        File file = new FileCreator().newFileFromPath("testFileWriterWriteLine");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.writeLine("test");
        fileWriter.closeWriter();

        FileReader fileReader = new FileReader(file);

        assertEquals("test", fileReader.readNextLine());

        fileReader.closeFile();
        file.delete();
    }

    @Test
    void testWriteLinesTrue() {
        File file = new FileCreator().newFileFromPath("testFileWriterWriteLines");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.writeLines(new String[] {"test", "", "test1"});
        fileWriter.closeWriter();

        FileReader fileReader = new FileReader(file);

        assertEquals(Arrays.toString(new String[] {"test", "", "test1"}), Arrays.toString(fileReader.readFile()));

        fileReader.closeFile();
        file.delete();
    }


}