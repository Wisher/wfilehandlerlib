package net.aliphaticus.wfilehandlerlib.utils;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class FileReaderTest {

    @Test
    void testIsScannerOpenTrue() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerOpenTrue");
        FileReader fileReader = new FileReader(file);

        assertTrue(fileReader.isScannerOpen());
        fileReader.closeFile();
        file.delete();
    }

    @Test
    void testIsScannerOpenFalse() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerOpenFalse");
        FileReader fileReader = new FileReader(file);
        fileReader.closeFile();

        assertFalse(fileReader.isScannerOpen());
        file.delete();
    }

    @Test
    void testCloseFileFalse() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerCloseFileFalse");
        FileReader fileReader = new FileReader(file);
        fileReader.closeFile();

        assertFalse(fileReader.isScannerOpen());
        file.delete();
    }

    @Test
    void testReadNextLineTrue() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerReadNextLineTrue");
        FileReader fileReader = new FileReader(file);
        FileWriter writer;
        try {
           writer = new FileWriter(file);
           writer.write("test");
           writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        assertEquals("test", fileReader.readNextLine());
        fileReader.closeFile();
        file.delete();
    }

    @Test
    void testReadFileTrue() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerReadFileTrue");
        FileReader fileReader = new FileReader(file);
        FileWriter writer;

        try {
            writer = new FileWriter(file);
            writer.write("test\n");
            writer.write("test2");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] expected = {"test", "test2"};
        String[] actual = fileReader.readFile();

        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        fileReader.closeFile();
        file.delete();
    }

    @Test
    void testReadRemainingFileTrue() {
        FileCreator fileCreator = new FileCreator();
        File file = fileCreator.newFileFromPath("testScannerReadRemainingFileTrue");
        FileReader fileReader = new FileReader(file);
        FileWriter writer;

        try {
            writer = new FileWriter(file);
            writer.write("test\n");
            writer.write("test2");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] expected = {"test2"};
        fileReader.readNextLine();
        String[] actual = fileReader.readRemainingFile();

        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
        fileReader.closeFile();
        file.delete();
    }
}