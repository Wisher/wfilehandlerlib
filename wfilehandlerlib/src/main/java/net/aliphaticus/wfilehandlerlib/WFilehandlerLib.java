/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib;

import java.io.File;

/**
 * Central FileHandler class with some static util methods
 */
public class WFilehandlerLib {


    /**
     * Obtain the working directory from the execution of the jar
     * @return - jar working directory
     */
    public static String getDirectory() {
        return System.getProperty("user.dir");
    }

    /**
     *
     * @param directory directory of the file
     * @param filename name of the file
     * @return True if file exists, false otherwise
     */
    public static Boolean exists(String directory, String filename) {
        File file = new File(directory + "/" + filename);
        return  file.exists();
    }

}
