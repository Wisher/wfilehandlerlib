/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib.utils;

import net.aliphaticus.wfilehandlerlib.WFilehandlerLib;

import java.io.File;
import java.io.IOException;

/**
 * Class containing methods for creating files
 */
public class FileCreator {

    /**
     * create a new file including the path
     * <p>Which can be obtained from System.getProperty("user.dir") or:</p>
     * <p>use {@link WFilehandlerLib#getDirectory() WFilehhander.GetDirectory} + the "\filename"<p>
     * @param FileName - String of the filename including path
     *
     */
    private static File createFile(String FileName) {
        File file = new File(FileName);
        boolean isCreated = false;
        try {
            if (file.exists()) {
                isCreated = true;
            } else {
                isCreated = file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // debug code
//        if (isCreated) {
//            System.out.println("created file: " + file.getAbsolutePath());
//        }

        if (!isCreated) {
            System.out.println("Error creating file: " + file.getAbsolutePath());
        }

        return file;
    }

    /**
     * create a new file from a specified FilePath, the directory must already exist
     * @param FilePath - FilePath as a string
     * @return the newly created file or existing file
     */
    public static File newFile(String FilePath) {
        return createFile(FilePath);
    }

    /**
     * create a new file in the specified directory, creates directory and parent directories if needed
     * @param Directory - String of the directory
     * @param FileName - string of the filename
     * @return new or existing file in the given directory
     */
    public static File newFile(String Directory, String FileName) {
        new File(Directory).mkdirs();
        return createFile(Directory + "/" + FileName);
    }

    /**
     * Generates a new file in the working directory
     * @param FileName - name of the file
     * @return newly created file in current directory
     */
    public static File newFileFromPath(String FileName) {
        return createFile(WFilehandlerLib.getDirectory() + "/" + FileName);
    }

}
