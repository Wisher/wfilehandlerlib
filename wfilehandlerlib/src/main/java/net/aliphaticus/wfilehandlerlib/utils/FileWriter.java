/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib.utils;

import java.io.File;
import java.io.IOException;

/**
 * Class for writing to files
 */
public class FileWriter {

    private java.io.FileWriter writer;
    private boolean isWriterOpen = false;
    private final File file;

    /**
     * Constructor requires a the file to be written to
     * @param file - File to be written to
     */
    public FileWriter(File file) {

        this.file = file;

        try {
            writer = new java.io.FileWriter(file);
            isWriterOpen = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Informs whether the writer is open
     * @return true if writer is open
     */
    public boolean isWriterOpen() {
        return isWriterOpen;
    }

    /**
     * Returns the {@link java.io.FileWriter} incase other native methods need to be accessed, such as {@link java.io.FileWriter#append(char)}
     * @return - FileWriter
     */
    public java.io.FileWriter getWriter() {
        return writer;
    }

    /**
     * Close the writer, this should be called when finished with the writer
     */
    public void closeWriter() {
        try {
            writer.close();
            isWriterOpen = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the writer to the start of the file
     * <p></p>Note that this will wipe the file of contents
     */
    public void resetWriter() {
        try {
            if (isWriterOpen) {
                closeWriter();
            }
            writer = new java.io.FileWriter(file);
            isWriterOpen = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes a string as a line into the file, appends a \n to the given string
     * @param line String to be written
     */
    public void writeLine(String line) {
        try {
            writer.write(line + "\n");
            writer.flush();
        } catch (IOException e) {
            System.out.println("Error writing to file: " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }

    /**
     * Same as {@link #writeLine(String)} but for writing an array of lines
     * <p></p> each element of the array is placed on its own line
     * @param lines Array of each line to be written
     */
    public void writeLines(String[] lines) {
        for (String line:
             lines) {
            try {
                writer.write(line + "\n");
            } catch (IOException e) {
                System.out.println("Error writing to file: " + file.getAbsolutePath());
                e.printStackTrace();
            }
        }

        try {
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
