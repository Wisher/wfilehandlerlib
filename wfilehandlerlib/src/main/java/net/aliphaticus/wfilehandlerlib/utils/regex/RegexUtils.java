/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib.utils.regex;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    /**
     * <p>Takes an inputted String, Pattern and "tail",
     * returns the matched pattern string.</p>
     *
     * <p>Returns "" if no Pattern is matched</p>
     *
     * @param pattern Pattern to find
     * @return String matching tail OR "" if no matches are located
     */
    public static String StringMatchTail(Pattern pattern, String input) {
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "";
        }
    }

}
