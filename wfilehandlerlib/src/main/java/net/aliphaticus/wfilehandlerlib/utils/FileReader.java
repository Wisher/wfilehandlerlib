/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class containing methods for reading files
 */
public class FileReader {

    private final File file;
    private Scanner scanner;
    private boolean isScannerOpen = false;

    /**
     * Object which reads files via a scanner
     * @param file - file to be read
     */
    public FileReader(File file) {

        this.file = file;

        try {
            scanner = new Scanner(file);
            isScannerOpen = true;
        } catch (FileNotFoundException e) {
            System.out.println("FileReader could not find the file: " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }

    /**
     *
     * @return - status of the scanner
     */
    public boolean isScannerOpen() {
        return this.isScannerOpen;
    }

    /**
     * closes the file, this should be called when done reading
     */
    public void closeFile() {
        scanner.close();
        isScannerOpen = false;
    }

    /**
     * opens the file, mainly used to reset the scanner the the top of file
     */
    private void  openFile() {
        if (!isScannerOpen) {
            try {
                scanner = new Scanner(file);
                isScannerOpen = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * checks if the file has another line to be scanned
     * @return - true if another line exists
     */
    private boolean hasNextLine() {

        if (!isScannerOpen) {
            openFile();
        }

        return scanner.hasNextLine();
    }

    /**
     * Advances the scanner forward a line and returns the line scanned
     * <p></p> returns null when the end of the file is reached
     * @return - line as a String
     */
    public String readNextLine() {

        if (!isScannerOpen) {
            openFile();
        }

        if (isScannerOpen) {
            if (hasNextLine()) {
                return scanner.nextLine();
            } else {
                return null;
            }
        } else {
            throw new IllegalStateException("Scanner is closed");
        }
    }

    /**
     * Scans and returns the whole file as a String[]
     * @return - String[] of the file
     */
    public String[] readFile() {
        return readFile(true);

    }

    /**
     * Core logic for {@link #readFile()} and {@link #readRemainingFile()}
     * <p></p> Scans the file and returns each line as a string in an array
     * <p></p> If input is true, scans from the start of the file, otherwise continues from current position
     * <p></p> returns an empty array if no lines exist
     * @param readFromStart - Should the file be read from the start
     * @return - String[] of the files contents
     */
    private String[] readFile(boolean readFromStart) {
        ArrayList<String> fileBuilder = new ArrayList<>();

        if (readFromStart) {
            closeFile();
            openFile();
        }

        if (!isScannerOpen) {
            openFile();
        }

        while (hasNextLine()) {
            fileBuilder.add(readNextLine());
        }

        String[] file = new String[fileBuilder.size()];
        return fileBuilder.toArray(file);
    }

    /**
     * Returns the remainder of the file from the scanner position as a String[]
     * @return - String[] of the files remaining contents
     */
    public String[] readRemainingFile() {
        return readFile(false);
    }


}
