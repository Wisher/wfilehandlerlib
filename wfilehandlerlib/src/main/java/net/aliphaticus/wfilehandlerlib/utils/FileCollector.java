/*
 * Copyright (c) 2022. Phillip MacNaughton <77wisher77@gmail.com>
 * All Rights Reserved unless otherwise explicitly stated
 */

package net.aliphaticus.wfilehandlerlib.utils;

import net.aliphaticus.wfilehandlerlib.WFilehandlerLib;
import net.aliphaticus.wfilehandlerlib.utils.regex.RegexUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Searches directories and returns arrays of files within the directory
 */
public class FileCollector {

    public static File[] filesInCurrentDir(int searchDepth) {
        return filesInDir(WFilehandlerLib.getDirectory(), searchDepth);
    }

    public static File[] filesInDir(String Directory, int searchDepth) {
        List<Path> filePaths;
        List<File> files = new ArrayList<>();
        File[] fileList = null;

        try {
            Stream<Path> fileNameStream = Files.walk(Paths.get(Directory), searchDepth);
            filePaths = fileNameStream.filter(file -> !Files.isDirectory(file)).collect(Collectors.toList());
            filePaths.forEach(
                    (v) -> files.add(
                            new File(String.valueOf(v))
                    )
            );
            fileList = new File[files.size()];
            fileList = files.toArray(fileList);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileList;
    }

    public static File[] filesInCurrentDirOfFileType(int searchDepth, Pattern FileName) {
        return filesInDirOfFileType(WFilehandlerLib.getDirectory(), searchDepth, FileName);
    }

    public static File[] filesInDirOfFileType(String Directory, int searchdepth, Pattern FileName) {
        File[] files = filesInDir(Directory, searchdepth);
        List<File> filesOfType = new ArrayList<>();

        for (File file : files) {
            String fileName = RegexUtils.StringMatchTail(FileName, file.getName());
            if (!fileName.equals("")) {
                filesOfType.add(file);
            }
        }

        File[] _files = new File[filesOfType.size()];
        _files = filesOfType.toArray(_files);

        return _files;
    }

}
